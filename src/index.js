const koa = require('koa');
const { initializeDatabase } = require('./db/database');
const { router: testersRouter } = require('./api/testers');
const app = new koa();

const PORT = 8725;

const startServer = async () => {
    await initializeDatabase();
    app.use(testersRouter.routes());
    app.use(testersRouter.allowedMethods());
    app.listen(PORT);
    // eslint-disable-next-line no-console
    console.log(`Server started on port: ${PORT}`);
    process.on('exit', () => {
        app.close();
        process.exit(0);
    })
}

if (!module.parent) {
    Promise.resolve().then(async () => {
        await startServer();
    });
}