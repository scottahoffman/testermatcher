**Tester Matcher**

Allows searching of testers based off desired values and presents them in a list.

How to run it:

1) Clone the repository: git clone https://scottahoffman@bitbucket.org/scottahoffman/testermatcher.git

1b) Make sure you have the correct versions of Node/NPM installed: 11.10.1/6.7.0 respectively.
I use NVM to manage Node versions: https://github.com/creationix/nvm for Unix OS or https://github.com/coreybutler/nvm-windows for Windows

2) cd into the cloned repository.

3) Build dependencies for backend server and react frontend: npm i && cd tester-matcher-client && npm i

4) Run the backend server (cd back to top level directory): npm start

5) Run the React app (cd tester-matcher-client): npm start

Once both are up and running you should be good to go!

How to use it:

Once the React app is up and running your browser should open a (very barebones) UI with multi select boxes that let you pick whatever countries or devices you want. If for some reason the window doesn't load, navigate to http://localhost:3000. When you pick at least one device and country option, it will automatically populate the data and update as your selections change.

You can also query the API directly at http://localhost:8725/api/testers. Some examples are below:

http://localhost:8725/api/testers?country=ALL&device=ALL
http://localhost:8725/api/testers?country=US&country=GB&device=iPhone%204&device=Galaxy%20S3

What's missing/What I want to add:

Tests - Mocha tests for both the front and backend. I would also add a couple end-to-end tests using protractor/Nightwatch.js.

Database - The backing database for this is an in-memory SQLite DB. Currently the schema is loaded through a series of hardcoded strings in `database.js` but it would be preferable to load it through a .sql file. In the past what I've had to do for that is parse the SQL file in code but it was a little out of scope here.

I'd absolutely want to move this to a real SQL DB, but didn't want to force users to install a database and run it for this simple example. Running in a MySQL DB would likely simplify the logic for creating the query by a lot. The SQLite driver doesn't support dynamic placeholders at all, and apparently at least the MySQL driver does (I didn't find this out until later).

API - It has non-existent error handling right now, and I'd absolutely want to make the API more resilient in that regard by adding standardized API errors for different conditions.

Frontend - The client is functional and responsive but the styling is extremely minimal. It needs a lot of work in that regard to make it more attractive.



