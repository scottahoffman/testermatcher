function search(query, cb) {
    // eslint-disable-next-line no-unused-vars
    let compiledQuery = '';
    for (const key of Object.keys(query)) {
      if (compiledQuery) compiledQuery += '&'
      compiledQuery += query[key].map((value) => `${key}=` + value).join('&'); 
    }
    
    return fetch(`api/testers?${compiledQuery}`, {
      accept: "application/json"
    })
      .then(checkStatus)
      .then(parseJSON)
      .then(cb);
  }
  
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    }
    const error = new Error(`HTTP Error ${response.statusText}`);
    error.status = response.statusText;
    error.response = response;
    throw error;
  }
  
  function parseJSON(response) {
    console.log(response);
    return response.json();
  }
  
  const Client = { search };
  export default Client;