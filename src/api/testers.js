const koaRouter = require('koa-router');
const { getTesters } = require('../db/database');

const router = new koaRouter();

router.get('/api/testers', async (ctx, next) => {
    await next();
    const queryParams = ctx.request.query;
    const selectedCountries = queryParams.country || [], selectedDevices = queryParams.device || []
    const resultTesters = await getTesters(selectedCountries, selectedDevices);
    ctx.body = resultTesters;
});

module.exports = { router };
