const csv = require('csvtojson')
const path = require('path');
const sqlite = require('sqlite3');
const db = new sqlite.Database(':memory:');
const BUG_FILE_PATH = './bugs.csv';
const DEVICES_FILE_PATH = './devices.csv';
const TESTER_DEVICE_FILE_PATH = 'tester_device.csv';
const TESTERS_FILE_PATH = 'testers.csv';

const CREATE_BUG_TABLE = `CREATE TABLE bugs (
    bugId INT PRIMARY KEY,
    deviceId INT,
    testerId INT,
    FOREIGN KEY (deviceId) REFERENCES devices(deviceId),
    FOREIGN KEY (testerId) REFERENCES testers(testerId)
    );`;
const CREATE_TESTERS_TABLE = `CREATE TABLE testers (
    testerId INT PRIMARY KEY,
    firstName TEXT,
    lastName TEXT,
    country TEXT,
    lastLogin TEXT
    );`;
const CREATE_TESTER_DEVICE = `CREATE TABLE testerDevice (
    testerId INT,
    deviceId INT,
    FOREIGN KEY (testerId) REFERENCES testers(testerId),
    FOREIGN KEY (deviceId) REFERENCES devices(deviceId)
    );`;
const CREATE_DEVICES_TABLE = `CREATE TABLE devices (
    deviceId INT PRIMARY KEY,
    description TEXT
    );`;

const CREATE_DESCRIPTION_INDEX = 'CREATE INDEX description_idx ON devices(description);';
const CREATE_COUNTRY_INDEX = 'CREATE INDEX country_idx ON testers(country);';

const INSERT_DEVICE = 'INSERT INTO devices (deviceId, description) VALUES (?, ?);';
const INSERT_BUG = 'INSERT INTO bugs (bugId, deviceId, testerId) VALUES (?, ?, ?);';
const INSERT_TESTERS = 'INSERT INTO testers (testerId, firstName, lastName, country, lastLogin) VALUES (?, ?, ?, ?, ?);';
const INSERT_TESTER_DEVICE = 'INSERT INTO testerDevice (testerId, deviceId) VALUES (?, ?);';

const dbSetupData = [
    { path: TESTERS_FILE_PATH, createQuery: CREATE_TESTERS_TABLE, insertQuery: INSERT_TESTERS, indexQuery: CREATE_COUNTRY_INDEX },
    { path: DEVICES_FILE_PATH, createQuery: CREATE_DEVICES_TABLE, insertQuery: INSERT_DEVICE, indexQuery: CREATE_DESCRIPTION_INDEX },
    { path: TESTER_DEVICE_FILE_PATH, createQuery: CREATE_TESTER_DEVICE, insertQuery: INSERT_TESTER_DEVICE },
    { path: BUG_FILE_PATH, createQuery: CREATE_BUG_TABLE, insertQuery: INSERT_BUG },
];

// SQLite library doesn't support being 'promisifed' through util promisify function, have to write custom wrappers to make it compatible
// with async/await or promises
db.allAsync = function (sql, params) {
    var that = this;
    return new Promise(function (resolve, reject) {
        that.all(sql, params, function (err, rows) {
            if (err)
                reject(err);
            else
                resolve(rows);
        });
    });
};

db.runAsync = function (sql, params) {
    var that = this;
    return new Promise(function (resolve, reject) {
        that.run(sql, params, function(err) {
            if (err)
                reject(err);
            else
                resolve();
        });
    })
};
// Creates the SQLite tables and loads the CSVs into them.
const initializeDatabase = async () => {
    for (const data of dbSetupData) {
        await db.runAsync(data.createQuery);
        const csvFileJson = await csv().fromFile(path.join(__dirname, data.path));
        for (const row of csvFileJson) {
            await db.runAsync(data.insertQuery, Object.values(row));
        }
        if (data.indexQuery) await db.runAsync(data.indexQuery);
    }
};

//The select query needs to be dynamically generated because there's no clean way to generate placeholders with SQLite
const createSelectQuery = (countries, devices) => {
    const selectQuery = `SELECT testers.testerId, testers.firstName, testers.lastName,
    (SUM(CASE WHEN bugs.testerId = testers.testerId AND (? = 'ALL' OR devices.description IN (${Array.isArray(devices) ? devices.map(() => '?').join(',') : '?'})) THEN 1 ELSE 0 END)) as bugCount
    FROM testers
    JOIN testerDevice ON testerDevice.testerId = testers.testerId
    JOIN devices ON devices.deviceId = testerDevice.deviceId
    JOIN bugs ON bugs.testerId = testers.testerId AND bugs.deviceId = testerDevice.deviceId
    WHERE (? = 'ALL' OR testers.country IN (${Array.isArray(countries) ? countries.map(() => '?').join(',') : '?'})) AND 
    (? = 'ALL' OR devices.description IN (${Array.isArray(devices) ? devices.map(() => '?').join(',') : '?'}))
    GROUP BY testers.testerId
    ORDER BY bugCount DESC;`;

    return selectQuery;
}

//Similarly need to generate the parameters for the SQL query to match the number of placeholders in the query
const generateSqlParameters = (countries, devices) => {
    const sqlParams = [devices];
    Array.isArray(devices) ? devices.map((device) => sqlParams.push(device)) : sqlParams.push(devices)
    sqlParams.push(countries);
    Array.isArray(countries) ? countries.map((country) => sqlParams.push(country)) : sqlParams.push(countries);
    sqlParams.push(devices);
    Array.isArray(devices) ? devices.map((device) => sqlParams.push(device)) : sqlParams.push(devices);
    
    return sqlParams;
}

const getTesters = async (countries, devices) => {
    const selectQuery = createSelectQuery(countries, devices);
    const sqlParams = generateSqlParameters(countries, devices);
    const resultSet = await db.allAsync(selectQuery, sqlParams);
    
    return resultSet;
}

module.exports = { initializeDatabase, getTesters };
