import React from "react";
import Client from "./Client";
import Select from 'react-select';

const MATCHING_ITEM_LIMIT = 10;
const countryOptions = [
  { value: 'US', label: 'United States' },
  { value: 'GB', label: 'United Kingdom' },
  { value: 'JP', label: 'Japan' },
  { value: 'ALL', label: 'All'}
];

const deviceOptions = [
  { value: 'iPhone 4', label: 'iPhone 4'},
  { value: 'iPhone 4S', label: 'iPhone 4S'},
  { value: 'iPhone 5', label: 'iPhone 5'},
  { value: 'Galaxy S3', label: 'Galaxy S3'},
  { value: 'Galaxy S4', label: 'Galaxy S4'},
  { value: 'Nexus 4', label: 'Nexus 4'},
  { value: 'Droid Razor', label: 'Droid Razor'},
  { value: 'Droid DNA', label: 'Droid DNA'},
  { value: 'HTC One', label: 'HTC One'},
  { value: 'iPhone 3', label: 'iPhone 3'},
  { value: 'ALL', label: 'All'},
]

class TesterSearch extends React.Component {

  state = {
    testers: [],
    country: [],
    device: [],
    selectedCountry: null,
    selectedDevice: null
  };

  launchSearch = value => {
    const { country, device } = this.state;
    if (value === "") {
      this.setState({
        testers: [],
      });
    } 
    Client.search({country, device}, testers => {
      this.setState({
        testers: testers.slice(0, MATCHING_ITEM_LIMIT)
      });
    });
  }

  handleCountryChange = async (selectedCountry) => {
    console.log(selectedCountry);
    let countries = selectedCountry;
    if (countries && countries.length > 0 && countries[0].value === 'ALL' && countries.length > 1) countries = countries.filter(item => item.value !== 'ALL');
    else if (countries && countries.length > 1 && countries[countries.length-1].value === 'ALL') countries = [countries[countries.length-1]];
    await this.setState({
      selectedCountry: countries,
      country: countries.map((country) => country.value),
    });
    console.log(this.state.country);
    this.launchSearch(this.state.country);
  };

  handleDeviceChange = async (selectedDevice) => {
    let devices = selectedDevice;
    if (devices && devices.length > 0 && devices[0].value === 'ALL' && devices.length > 1) devices = devices.filter(item => item.value !== 'ALL');
    else if (devices && devices.length > 1 && devices[devices.length-1].value === 'ALL') devices = [devices[devices.length-1]];
    await this.setState({
      selectedDevice: devices,
      device: devices.map((device) => device.value),
    });
    console.log(this.state.device);
    this.launchSearch(this.state.device);
  };

  render() {
    const { testers, selectedCountry, selectedDevice } = this.state;

    const testerRows = testers.map((tester) => (
      <tr>
        <td>{tester.firstName} {tester.lastName}</td>
        <td>{tester.bugCount}</td>
      </tr>
    ));

    return (
      <div id="tester-search">
        <h1>{'Tester Matcher'}</h1>
        <Select
          placeholder={'Select countries...'}
          value={selectedCountry}
          onChange={this.handleCountryChange}
          options={countryOptions}
          isMulti
          isSearchable
        />
        <Select
          placeholder={'Select devices...'}
          value={selectedDevice}
          onChange={this.handleDeviceChange}
          options={deviceOptions}
          isMulti
          isSearchable
        />
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Experience</th>
            </tr>
          </thead>
          <tbody>
            {testerRows}
          </tbody>
        </table>
      </div>
    );
  }
}

export default TesterSearch;