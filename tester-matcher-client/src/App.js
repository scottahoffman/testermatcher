import React, { Component } from "react";
import TesterSearch from "./TesterSearch";

class App extends Component {
  state = {
    testers: []
  };

  render() {
    const { testers } = this.state;

    return (
      <div className="App">
        <div className="ui text container">
          <TesterSearch
          testers={testers}
          />
        </div>
      </div>
    );
  }
}

export default App;